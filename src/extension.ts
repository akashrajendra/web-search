'use strict';
import { ExtensionContext, commands, window, Uri, workspace } from 'vscode';

export function activate(context: ExtensionContext) {
	var previousVersion = context.globalState.get('websearch.isFirstInstall')
	if (!previousVersion) {
		window.showInformationMessage('Welcome to Web Search!')
		context.globalState.update('websearch.isFirstInstall', true)
	}

	context.subscriptions.push(commands
		.registerCommand('extension.websearch.SearchGoogle', () => {
			Search('Google');
		})
	);

	context.subscriptions.push(commands
		.registerCommand('extension.websearch.SearchDuckDuckGo', () => {
			Search('DuckDuckGo');
		})
	);

	context.subscriptions.push(commands
		.registerCommand('extension.websearch.SearchStackOverflow', () => {
			Search('StackOverflow');
		})
	);

	context.subscriptions.push(commands
		.registerCommand('extension.websearch.Searchcode', () => {
			Search('Searchcode');
		})
	);

	context.subscriptions.push(commands
		.registerCommand('extension.websearch.SearchNPM', () => {
			Search('NPM');
		})
	);

	function Search(Engine: string) {
		const editor = window.activeTextEditor;
		const shouldAppendFT: boolean = workspace.getConfiguration().get('websearch.appendLanguageID');
		let query: string = editor.document.getText(editor.selection);
		let SOQuery: string = query;
		let NPMQuery: string = query;
		if (shouldAppendFT == true) {
			query = editor.document.languageId + ' ' + query;
			SOQuery = '[' + editor.document.languageId + '] ' + SOQuery;
		}
		switch (Engine) {
			case 'Google':
				commands.executeCommand('vscode.open', Uri.parse('https://www.google.com/search?q=' + query));
				break;
			case 'DuckDuckGo':
				commands.executeCommand('vscode.open', Uri.parse('https://duckduckgo.com/?q=' + query));
				break;
			case 'StackOverflow':
				commands.executeCommand('vscode.open', Uri.parse('https://stackoverflow.com/search?q=' + SOQuery));
				break;
			case 'Searchcode':
				commands.executeCommand('vscode.open', Uri.parse('https://searchcode.com/?q=' + query));
				break;
			case 'NPM':
				commands.executeCommand('vscode.open', Uri.parse('https://www.npmjs.com/search?q=' + query));
				break;
			default: console.log('This should NOT happen');
		}
	}
}

export function deactivate() {}

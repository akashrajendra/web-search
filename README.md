# web-search

Search Google, DuckDuckGo, StackOverflow and more using default browser!

![demo](resources/demo.gif)

It can append [languageID](https://code.visualstudio.com/docs/languages/identifiers) to the search query automatically, be it JavaScript or HTML, or disable it completely if you don't like it!

![StackOverflow auto-tag](resources/so-ss.png)

## Extension Configuration

`websearch.appendLanguageID`: **boolean** Sets whether you want Web Search to append languageID to the search query

## Release Notes

### 0.0.1

Initial release

[MIT](http://opensource.org/licenses/MIT) © 2018 Akash Rajendra
